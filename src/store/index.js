import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    count:0,
    ships: []
  },
  getters: {
    getShip: state => index => state.ships[index]
    // getShip(state){
    //   return function (index){
    //     return state.ships[index]
    //   }
    // },
    // getShips(state){
    //   return state.ships[0]
    // }
  },
  mutations: {
    changeCounter(state, n){
      state.count+= n
    },
    setShips(state, shipsToSave){
      state.ships = shipsToSave
    }
  },
  actions: {
    async fetchShips(context){
      const response = await Axios.get("https://swapi.dev/api/starships")
      const ships = response.data.results;
      context.commit("setShips", ships)
    }
  },
  modules: {
  },
  strict: true
})
