import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import IndexShip from '@/views/IndexShip.vue';
import ShowShip from '@/views/ShowShip.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/ships',
    name: 'IndexShip',
    component: IndexShip
  },
  {
    path: '/ships/:id',
    name: 'ShowShip',
    component: ShowShip
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
